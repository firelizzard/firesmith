let fs = require('fs-extra'),
    path = require('path-extra')

let { Middleware } = require('./middleware')
let Pipeline = require('./pipeline')

// load named files
class LoadFileMiddleware extends Middleware {
    constructor(base) {
        super()
        this.base = base
    }
  
    async invoke(rsc, next) {
        let fullPath = path.join(this.base, rsc.name)
        rsc.content = await fs.readFile(fullPath, 'utf8')
    
        return await next(rsc)
    }
}

class SaveFileMiddleware extends Middleware {
    constructor(base) {
        super()
        this.base = base
    }

    async invoke(rsc, next) {
        if (!rsc.destination)
            throw new Error('no destination specified')

        // get the URL
        let writePath = path.join(this.base, rsc.destination)

        // Ensure that the output directory exists
        let dirPath = path.dirname(writePath)
        if (!await fs.exists(dirPath))
            await fs.mkdirp(dirPath)

        // Output using fs-extra:
        await fs.outputFile(writePath, rsc.content)

        return await next(rsc)
    }
}

class FilePipeline extends Pipeline {
    loadFiles(base) {
        return this.use(new LoadFileMiddleware(base))
    }

    saveFiles(base) {
        return this.use(new SaveFileMiddleware(base))
    }
}

module.exports = { LoadFileMiddleware, SaveFileMiddleware, FilePipeline }