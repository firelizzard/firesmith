const { Middleware, BranchAnyMiddleware } = require('./middleware')

class RecipeBase {
    constructor(steps) {
        this.steps = steps

        this.isStringArray =
            steps instanceof Array &&
            steps.filter(x => typeof x === 'string').length === steps.length
    }

    makeHelpers(pipeline, config) {
        return { applyOwn, apply }
        
        function applyOwn(i, step, alterations) {
            if (step === true && typeof i === 'string')
                return pipeline[i](config)

            if (typeof alterations === 'object' && step instanceof RecipeBase)
                return step.apply(pipeline, config, alterations)

            if (apply(step))
                return
                
            throw new Error(`Failed to apply step ${i}`)
        }

        function apply(step, ownStep) {
            if (step === void 0 || step === null)
                return false

            if (typeof step === 'string') {
                pipeline[step](config)
                return true
            }

            if (step instanceof Middleware) {
                pipeline.use(step)
                return true
            }

            if (step instanceof RecipeBase) {
                step.apply(pipeline, config)
                return true
            }
            
            if (typeof step !== 'function')
                return false

            if (step.prototype instanceof Middleware) {
                apply(new step(config))
                return true
            }

            if (step.prototype instanceof RecipeBase) {
                apply(new step())
                return true
            }

            step(pipeline, config, ownStep)
            return true
        }
    }

    get(i, alterations) {
        let step = this.steps[i]
        let key = this.isStringArray ? step : i
        let alt = alterations[key]

        return { step, key, alt }
    }

    apply(pipeline, config, alterations) {
        throw new Error('not implemented')
    }
}

class SequenceRecipe extends RecipeBase {
    apply(pipeline, config, alterations) {
        let { apply, applyOwn } = this.makeHelpers(pipeline, config)

        if (typeof alterations !== 'object') {
            for (let i of Object.keys(this.steps))
                applyOwn(i, this.steps[i])
            return
        }

        for (let i of Object.keys(this.steps)) {
            let { step, alt } = this.get(i, alterations)
            if (alt === false)
                continue

            apply(alt, step) || applyOwn(i, step, alt)
        }
    }
}

class BranchRecipe extends RecipeBase {
    apply(pipeline, config, alterations) {
        let setupFns = Array.from(
            typeof alterations === 'object' ?
                this.altBranches(config, alterations) :
                this.ownBranches(config))

        pipeline.branchAny(setupFns)
    }

    * ownBranches(config) {
        for (let i of Object.keys(this.steps))
            yield pl => {
                let { applyOwn } = this.makeHelpers(pl, config)
                applyOwn(i, this.steps[i])
            }
    }

    * altBranches(config, alterations) {
        for (let i of Object.keys(this.steps)) {
            let { step, alt } = this.get(i, alterations)

            if (alt === false)
                continue

            yield pl => {
                let { apply, applyOwn } = this.makeHelpers(pl, config)
                apply(alt) || applyOwn(i, step, alt)
            }
        }
    }
}

module.exports = {
    Base: RecipeBase,
    Sequence: SequenceRecipe,
    Branch: BranchRecipe
}