const path = require('path-extra')
const fs = require('fs-extra')
const globby = require('globby')

class Resource {
    constructor(name) {
        let kind = this.constructor.name.toLowerCase()
        if (kind != 'resource' && kind.endsWith('resource'))
            kind = kind.substring(0, kind.length - 8)

        this.kind = kind
        this.name = name
        this.path = path.removeExt(name)
        this.id = `${this.kind}:${this.path}`
        this.extension = path.extname(name)
    }

    dependencies(stage) {
        return []
    }
}

class ResourceManager {
    constructor(kind = null) {
        if (typeof kind === 'function' && kind.prototype && kind.prototype.constructor === kind)
            this.kind = kind
        else if (kind != null)
            throw new Error('invalid resource kind')
    }

    manages(resource) {
        if ('kind' in this)
            return resource instanceof this.kind
        
        throw new Error('not implemented')
    }

    paths(config) {
        throw new Error('not implemented')
    }

    async gather(config) {
        throw new Error('not implemented')
    }

    async buildPipelines(config, alterations) {
        throw new Error('not implemented')
    }

    async gatherFiles(config, action) {
        const { base, patterns } = this.paths(config)

        await fs.access(base)

        const files = globby(patterns, { cwd: base })

        return action ? action(await files) : files
    }
}



module.exports = { Resource, ResourceManager }