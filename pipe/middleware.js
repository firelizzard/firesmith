class Middleware {
  // invoke executes the middleware and returns a promise
  // parameters are data and a function to call the next middleware
  async invoke(data, next) {
    throw new Error('not implemented')
  }
}

class DelegateMiddleware extends Middleware {
  constructor(invoke) {
    super();
    this.invoke = invoke;
  }
}

// attempt multiple branches; stop at the first that returns something
class BranchAnyMiddleware extends Middleware {
  constructor(branches) {
    super();
    this.branches = branches
  }

  invoke(rsc, next) {
    return trybr(0, i => {
      if (i < this.branches.length)
        return this.branches[i].bind(null, rsc, next)
      
      throw new Error('No branches succeeded')
    })

    async function trybr(index, get) {
      let step = get(index)

      try {
        return await step()
      } catch (e) {
        if (!(e instanceof FilterRejection))
          throw e
      }

      return trybr(index + 1, get);
    }
  }
}

class FilterRejection extends Error {}

// conditional execution for middleware
class FilterMiddleware extends Middleware {
  constructor(message) {
    super();

    this.message = message || 'condition failed';
  }

  // override condition to do something useful
  condition(rsc) {
    return true;
  }

  // passed defaults to calling the next middleware
  passed(rsc, next) {
    return next(rsc);
  }

  // failed defaults to terminating the pipeline
  failed(rsc, next) {
    return Promise.reject(new FilterRejection(this.message));
  }

  invoke(rsc, next) {
    if (this.condition(rsc))
      return this.passed(rsc, next);
    else
      return this.failed(rsc, next);
  }
}

// filter on file extensions
class ExtensionMiddleware extends FilterMiddleware {
  constructor(ext) {
    super('extension did not match');
    
    if (typeof ext === 'string')
      this.filter = x => x === ext;
    else if (typeof ext === 'function')
      this.filter = ext;
    else if (ext instanceof Array)
      this.filter = x => ext.indexOf(x) >= 0;
    else
      throw new Error('invalid argument: ext');
  }

  condition(rsc) {
    return this.filter(rsc.extension)
  }
}

module.exports = {
    Middleware,
    DelegateMiddleware,
    BranchAnyMiddleware,
    FilterMiddleware,
    ExtensionMiddleware,
    FilterRejection
}