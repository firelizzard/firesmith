let minimatch = require('minimatch'),
    globby = require('globby')

{
    let orig = minimatch
    minimatch = function minimatch(target, pattern, options) {
        // convert 'kind:name' to 'kind://name' so that matches work properly
        return orig(target.replace(':', '://'), pattern.replace(':', '://'), options)
    }
}

class Coordinator {
    constructor(handlers) {
        this.handlers = handlers
    }

    static createHandler(managerCls, config, alterations) {
        let manager = new managerCls()
        let pipelines = manager.buildPipelines(config, alterations)

        return { manager, pipelines }
    }

    paths(config) {
        return this.handlers.map(x => x.manager.paths(config)).filter(x => x)
    }

    async gather(config) {
        const { ignore = [] } = config
        return await Promise
            .all(this.handlers.map(x => x.manager.gather(config)))
            .then(function* (iters) {
                for (let iter of iters)
rscs:               for (let resource of iter) {
                        for (let pattern of ignore)
                            if (minimatch(resource.name, pattern))
                                continue rscs
                        yield resource
                    }
            })
    }

    execute(stage, resources) {
        // exclude skipped resources
        const graph = new DependencyGraph(resources.filter(x => x))

        graph.build(stage) // build graph/resolve dependencies
        graph.acyclic() // ensure no cyclic dependencies
        return graph.process(this.handlers, stage) // process pipelines
    }
}

class DependencyGraph {
    constructor(resources) {
        for (const resource of resources)
            this[resource.id] = new ResourceDependencyInformation(resource)
    }

    build(stage) {
        for (const name in this) this[name].build(stage, this)
    }
    
    acyclic() {
        check = check.bind(this)

        for (const name in this) check(name)

        function check(name, chain) {
            for (let link = chain; link; link = link.prev) {
                if (link.name === name) throw new Error('detected cyclic dependency')
            }

            chain = { name, prev: chain }

            const node = this[name]
            for (const dependency of node.dependencies.values())
                if (!dependency.previous)
                    check(dependency.name, chain)
        }
    }

    * process(handlers, stage) {
        for (const name in this) yield this[name].process(handlers, stage, this)
    }
}

class ResourceDependencyInformation {
    constructor(resource) {
        this.resource = resource
        this.dependencies = new Map()

        this.promise = new Promise(r => this.complete = r)
    }

    build(stage, graph) {
        for (const dependency of this.resource.dependencies(stage))
            resolve.call(this, dependency)

        function resolve(dependency) {
            let required = false, previous = false
            
        prefixLoop:
            while (true) {
                switch (dependency[0]) {
                    case '!': required = true; break
                    case '^': previous = true; break
                    default:                   break prefixLoop
                }
                
                dependency = dependency.substring(1)
            }
            
            // if the dependency doesn't specify a resource type, default to the same kind
            if (!/^[^/]+:/.test(dependency))
                dependency = `${this.resource.kind}:${dependency}`
        
            // check if the dependency is explicit and within the graph
            if (dependency in graph) {
                this.relate(new DependencyRelation(dependency, required, previous))
                return
            }
        
            if (!globby.hasMagic(dependency)) {
                if (required)
                    throw new Error(`missing dependency: ${this.resource.id} requires '${dependency}'`)
                return
            }
        
            let found = false
            for (const name in graph) {
                if (this.resource.id == name) continue
                if (!minimatch(name, dependency)) continue
        
                this.relate(new DependencyRelation(name, required, previous))
                found = true
            }
        
            if (!found && required)
                throw new Error(`missing dependency: ${this.resource.id} requires '${dependency}'`)
        }
    }

    relate(relation) {
        if (this.dependencies.has(relation.name)) return false

        this.dependencies.set(relation.name, relation)
    }

    async process(handlers, stage, graph) {
        const dependencies = Array.from(this.dependencies.values())
        const resolved = await Promise.all(dependencies.map(x => x.resolve(graph)))

        this.resource.deps = resolved
        const result = handle(handlers, stage, this.resource)
        this.complete(result)

        try {
            await result
        } catch (error) {
            console.error(`error while processing ${this.resource.id}: ${error.message}`)
            console.error(error)
        }
        return this.resource

        function handle(handlers, stage, rsc) {
            for (let handler of handlers) {
                if (!handler.manager.manages(rsc))
                    continue
        
                return handler.pipelines[stage](rsc)
            }
        }
    }
}

class DependencyRelation {
    constructor(name, required, previous) {
        Object.assign(this, { name, required, previous })
    }

    resolve(graph) {
        const { resource, promise } = graph[this.name]
        return this.previous ? resource : promise
    }
}

module.exports = Coordinator