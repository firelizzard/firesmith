let path = require('path-extra'),
    jstransformer = require('jstransformer'),
    inputformat = require('inputformat-to-jstransformer').dictionary

const { Middleware } = require('../pipe/middleware');
const Pipeline = require('../pipe/pipeline')
const Recipe = require('../pipe/recipe')
const { Resource, ResourceManager } = require('../pipe/resource')
const { FilePipeline } = require('../pipe/basics')

class LayoutResource extends Resource {
}

class LayoutResourceManager extends ResourceManager {
    constructor() {
        super(LayoutResource)
    }

    paths(config) {
        const globs = ['**/*']
        if (config.ignore)
            globs.push(...config.ignore.map(x => `!${x}`))

        return {
            base: config.layouts,
            patterns: globs
        }
    }

    async gather(config) {
        return await this.gatherFiles(config, function * (names) {
            for (let name of names)
                yield new LayoutResource(name)
        })
    }

    buildPipelines(config, alterations) {
        let pre = new PreprocessPipeline()
        let process = new ProcessPipeline()

        if (alterations) {
            pre.useDefaults(config, alterations.pre)
            process.useDefaults(config, alterations.process)
        } else {
            pre.useDefaults(config)
            process.useDefaults(config)
        }

        return {
            pre: pre.build(),
            process: process.build(),
            post: Pipeline.noop
        }
    }
}

class PreprocessPipeline extends FilePipeline {
    useDefaults(config, alterations) {
        new Recipe.Sequence({
            logErrors: true,
            loadFiles: (pl, cfg) => pl.loadFiles(cfg.layouts),
        }).apply(this, config, alterations)
        return this
    }
}

class ProcessPipeline extends Pipeline {
    useDefaults(config, alterations) {
        new Recipe.Sequence([
            'logErrors',
            'compileLayout'
        ]).apply(this, config, alterations)
        return this
    }

    compileLayout(config) {
        return this.use(new LayoutCompilerMiddleware(config))
    }
}

class LayoutCompilerMiddleware extends Middleware {
    constructor(config) {
        super()
        this.config = config
    }

    get(fmt) {
        for (let name of inputformat[fmt])
            try {
                return (this.config.require || require)(name)
            } catch (error) {
                if (!error || !error.message || !error.message.startsWith('Cannot find module '))
                    throw error
                continue
            }
    }

    async invoke(rsc, next) {
        let fmt = rsc.extension.substring(1)
        if (!(fmt in inputformat))
            throw new Error(`No jstransformer found for the format "${fmt}"`)

        let transformer = this.get(fmt)
        if (!transformer)
            throw new Error(`No jstransformer installed for the format "${fmt}"`)

        let jst = jstransformer(transformer)
        let opts = Object.assign({
            filename: path.join(this.config.layouts, rsc.name)
        }, this.config[transformer.name])
        let compiled = await jst.compileAsync(rsc.content, opts)
        rsc.content = x => compiled.fn(x)

        return await next(rsc)
    }
}

module.exports = LayoutResourceManager
module.exports.PreprocessPipeline = PreprocessPipeline
module.exports.ProcessPipeline = ProcessPipeline