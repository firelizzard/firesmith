let path = require('path-extra'),
    grayMatter = require('gray-matter'),
    marked = require('marked'),
    cloneDeep = require('lodash/cloneDeep'),
    defaultsDeep = require('lodash/defaultsDeep')

const { Middleware, FilterMiddleware, ExtensionMiddleware } = require('../pipe/middleware')
const Pipeline = require('../pipe/pipeline')
const Recipe = require('../pipe/recipe')
const { Resource, ResourceManager } = require('../pipe/resource')
const { FilePipeline } = require('../pipe/basics')

class ContentResource extends Resource {
    dependencies(stage) {
        if (stage == 'process') {
            let deps = []
            if (this.data._pages)
                deps.push(...this.data._pages)
            let p = this.path
            while (p.indexOf('/') >= 0) {
                p = path.dirname(p)
                deps.push(`defaults:${p}/_defaults`)
            }
            deps.push('defaults:_defaults')
            return deps
        }

        if (stage == 'post') {
            let deps = []
            if (this.data._layout)
                deps.push(`!layout:${this.data._layout}`)
            return deps
        }

        return []
    }
}

class ContentResourceManager extends ResourceManager {
    constructor() {
        super(ContentResource)
    }

    paths(config) {
        const globs = ['**/*.@(html|md|markdown)']
        if (config.ignore)
            globs.push(...config.ignore.map(x => `!${x}`))

        return {
            base: config.src,
            patterns: globs
        }
    }

    async gather(config) {
        return await this.gatherFiles(config, function * (names) {
            for (let name of names)
                yield new ContentResource(name)
        })
    }

    buildPipelines(config, alterations) {
        let pre = new PreprocessPipeline()
        let render = new RenderPipeline()
        let post = new PostprocessPipeline()

        if (alterations) {
            pre.useDefaults(config, alterations.pre)
            render.useDefaults(config, alterations.render)
            post.useDefaults(config, alterations.post)
        } else {
            pre.useDefaults(config)
            render.useDefaults(config)
            post.useDefaults(config)
        }

        return {
            pre: pre.build(),
            process: render.build(),
            post: post.build()
        }
    }
}

class PreprocessPipeline extends FilePipeline {
    useDefaults(config, alterations) {
        new Recipe.Sequence({
            logErrors: true,
            loadFiles: (pl, cfg) => pl.loadFiles(cfg.src),
            processGrayMatter: true,
        }).apply(this, config, alterations)
        return this
    }

    processGrayMatter() {
        return this.use(new GrayMatterMiddleware())
    }
}

class RenderPipeline extends Pipeline {
    useDefaults(config, alterations) {
        new Recipe.Sequence({
            logErrors: true,
            ignoreFilterRejection: true,
            getDefaults: true,
            collectDependencies: true,
            excludeDrafts: !config.draft,
            render: new Recipe.Branch({
                plain: 'passthroughPlain',
                markdown: 'renderMarkdownWithMarked',
                html: 'passthroughHtml'
            }),
        }).apply(this, config, alterations)
        return this
    }

    getDefaults() {
        return this.use(new DefaultsMiddleware())
    }

    collectDependencies() {
        return this.useDelegate(async (rsc, next) => {
            rsc.pages = rsc.deps.filter(x => x && x.kind == 'content')
            return await next(rsc)
        })
    }

    excludeDrafts() {
        return this.use(new ExcludeDraftsMiddleware())
    }

    passthroughPlain() {
        return this.use(new PassthroughPlainMiddleware())
    }

    passthroughHtml() {
        return this.use(new PassthroughHtmlMiddleware())
    }

    renderMarkdownWith(render) {
        return this.use(new RenderMarkdownMiddleware(render))
    }

    renderMarkdownWithMarked() {
        return this.renderMarkdownWith(marked)
    }
}

class PostprocessPipeline extends FilePipeline {
    useDefaults(config, alterations) {
        new Recipe.Sequence({
            logErrors: true,
            renderLayouts: true,
            targetHtml: true,
            saveFiles: (pl, cfg) => pl.saveFiles(cfg.dist)
        }).apply(this, config, alterations)
        return this
    }

    renderLayouts(config) {
        return this.use(new RenderLayoutMiddleware(config))
    }

    targetHtml(config) {
        return this.use(new HtmlTargetMiddleware())
    }
}

// extract front-matter from files
class GrayMatterMiddleware extends Middleware {
    async invoke(rsc, next) {
        let gray = grayMatter(rsc.content)

        // Need to perform a full clone of this object since gray-matter does some
        // agressive caching, and our mutation can mess up the cache
        rsc.data = cloneDeep(gray.data)

        // get rid of the front-matter
        rsc.content = gray.content

        return await next(rsc)
    }
}

class DefaultsMiddleware extends Middleware {
    async invoke(rsc, next) {
        let deps = rsc.deps.filter(x => x && x.kind == 'defaults')
        let defaults = deps.length ? deps[0].content : {}
        defaultsDeep(rsc.data, defaults)
        return await next(rsc)
    }
}

class ExcludeDraftsMiddleware extends FilterMiddleware {
    constructor() {
        super('draft')
    }

    condition(rsc) {
        return !rsc.data._draft
    }
}

class PassthroughPlainMiddleware extends FilterMiddleware {
    condition(rsc) {
        return rsc.data._render == false
    }
}

class PassthroughHtmlMiddleware extends ExtensionMiddleware {
    constructor() {
        super(['.html', '.htm'])
    }
}

// render markdown files
class RenderMarkdownMiddleware extends ExtensionMiddleware {
    constructor(render) {
        super(['.md', '.markdown'], render)
        this.render = render
    }

    async passed(rsc, next) {
        rsc.content = await this.render(rsc.content)
        return await next(rsc)
    }
}

// for files that specify a layout, render them using it for other files, passthrough
class RenderLayoutMiddleware extends FilterMiddleware {
    constructor(config) {
        super()
        this.config = config
    }

    condition(rsc) {
        return rsc.deps.filter(x => x.kind == 'layout').length
    }

    async passed(rsc, next) {
        let layout = rsc.deps.filter(x => x.kind == 'layout')[0]
        let locals = Object.assign({
            _body: rsc.content,
            _path: rsc.path,
            _ext: rsc.extension,
        }, rsc.data, {
            _pages: rsc.pages
        })
        let res = await layout.content(locals)
        rsc.content = res

        return await next(rsc)
    }

    async failed(rsc, next) {
        return await next(rsc)
    }
}

class HtmlTargetMiddleware extends Middleware {
    async invoke(rsc, next) {
        rsc.destination = this.getPath(rsc)
        return await next(rsc)
    }

    getPath(rsc) {
        if (!rsc.data._location)
            return `${rsc.path}.html`

        let dir = path.dirname(rsc.path)
        return path.join(dir, rsc.data._location)
    }
}

module.exports = ContentResourceManager
module.exports.Pipelines = { PreprocessPipeline, RenderPipeline, PostprocessPipeline }
module.exports.Middleware = { RenderMarkdownMiddleware }