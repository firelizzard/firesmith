let path = require('path-extra'),
    yaml = require('js-yaml'),
    defaultsDeep = require('lodash/defaultsDeep')

const { Middleware } = require('../pipe/middleware');
const Pipeline = require('../pipe/pipeline')
const Recipe = require('../pipe/recipe')
const { Resource, ResourceManager } = require('../pipe/resource')
const { FilePipeline } = require('../pipe/basics')

class DefaultsResource extends Resource {
    dependencies(stage) {
        if (stage == 'process') {
            if (this.path.indexOf('/') < 0)
                return []

            let deps = []
            let p = path.dirname(this.path)
            while (p.indexOf('/') >= 0) {
                p = path.dirname(p)
                deps.push(p + '/_defaults')
            }
            deps.push('_defaults')
            return deps
        }

        return []
    }
}

class DefaultsResourceManager extends ResourceManager {
    constructor() {
        super(DefaultsResource)
    }

    paths(config) {
        const globs = [`**/${config.defaults || '_defaults'}.@(yaml|yml|json)`]
        if (config.ignore)
            globs.push(...config.ignore.map(x => `!${x}`))

        return {
            base: config.src,
            patterns: globs
        }
    }

    async gather(config) {
        return await this.gatherFiles(config, function * (names) {
            for (let name of names)
                yield new DefaultsResource(name)
        })
    }

    buildPipelines(config, alterations) {
        let pre = new PreprocessPipeline()
        let process = new ProcessPipeline()

        if (alterations) {
            pre.useDefaults(config, alterations.pre)
            process.useDefaults(config, alterations.process)
        } else {
            pre.useDefaults(config)
            process.useDefaults(config)
        }

        return {
            pre: pre.build(),
            process: process.build(),
            post: Pipeline.noop
        }
    }
}

class PreprocessPipeline extends FilePipeline {
    useDefaults(config, alterations) {
        new Recipe.Sequence({
            logErrors: true,
            loadFiles: (pl, cfg) => pl.loadFiles(cfg.src),
        }).apply(this, config, alterations)
        return this
    }
}

class ProcessPipeline extends Pipeline {
    useDefaults(config, alterations) {
        new Recipe.Sequence([
            'logErrors',
            'processYaml'
        ]).apply(this, config, alterations)
        return this
    }

    processYaml() {
        return this.use(new ProcessYamlMiddleware());
    }
}

class ProcessYamlMiddleware extends Middleware {
    async invoke(rsc, next) {
        const parent = rsc.deps[0] && rsc.deps[0].content || {}
        try {
            const data = yaml.safeLoad(rsc.content)
            rsc.content = defaultsDeep({}, data, parent)
        } catch (error) {
            // ensure that content is set to prevent exceptions later down the road
            rsc.content = defaultsDeep({}, parent)
            throw error
        }
        return await next(rsc)
    }
}

module.exports = DefaultsResourceManager
module.exports.PreprocessPipeline = PreprocessPipeline
module.exports.ProcessPipeline = ProcessPipeline