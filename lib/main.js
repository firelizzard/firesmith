let Coordinator = require('../pipe/coordinator'),
    Content = require('./content'),
    Defaults = require('./defaults'),
    Layout = require('./layout')

module.exports = function init(config, alterations) {
    const handlers = [
        Coordinator.createHandler(Content, config, alterations.content),
        Coordinator.createHandler(Defaults, config, alterations.defaults),
        Coordinator.createHandler(Layout, config, alterations.layout)
    ]
    
    if (alterations.init)
        for (const manager of alterations.init)
            handlers.push(Coordinator.createHandler(manager, config))

    return new Coordinator(handlers)
}