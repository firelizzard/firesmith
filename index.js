let init = require('./lib/main'),
    minimatch = require('minimatch'),
    path = require('path'),
    chokidar = require('chokidar')


Object.assign(module.exports = execute, {
    execute,
    watch
})

async function execute(config, alterations = {}) {
    let ctrl = init(config, alterations)
    await run(ctrl, config)
}

async function watch(config, alterations = {}) {
    let ctrl = init(config, alterations)
    await run(ctrl, config)

    const paths = ctrl.paths(config)
    const base = getBasePath(paths.map(x => x.base))
    const watcher = chokidar.watch(base, {
        awaitWriteFinish: true,
        ignoreInitial: true,
    })

    watcher.on('all', async (event, file) => {
        let best = null
        for (const { base, patterns } of paths) {
            // get path relative to base
            const rel = path.relative(base, file)

            // ignore anything outside of base
            if (/\.\.(\\|\/|$)/.test(rel)) continue

            // path set does not contain this file
            if (!matches(rel, patterns)) continue

            const len = base.split(/[\\\/]+/).length
            if (best == null || best.len < len)
                best = { base, rel, len }
        }

        if (best == null) return
        const { base, rel } = best

        if (!config.includeDot) {
            if (minimatch(rel, '.*')) return
            if (minimatch(rel, '.*/**')) return
        } else if (!config.includeGit) {
            if (minimatch(rel, '.git')) return
            if (minimatch(rel, '.git/**')) return
        }

        console.log(`file changed within "${base}": "${rel}"`)
        await run(ctrl, config)
    })

    // fswatch([config.src, config.layouts], options, async (event, path) => {
    //     await run(ctrl, config)
    // })
}

async function run(ctrl, config) {
    if (config.prerun)
        config.prerun(ctrl, config)

    let r1 = Array.from(await ctrl.gather(config))
    let r2 = await Promise.all(ctrl.execute('pre', r1))
    let r3 = await Promise.all(ctrl.execute('process', r2))
    await Promise.all(ctrl.execute('post', r3))
    
    if (config.postrun)
        config.postrun(ctrl, config)
}

function getBasePath(paths) {
    const segmented = paths.map(x => x.split(/[\\/]+/))

    let i
    for (i = 0; ; i++) {
        if (segmented.filter(x => i >= x.length).length) break
        
        if (!match(segmented.map(x => x[i]))) break
    }
    
    return segmented[0].slice(0, i).join(path.sep)

    function match(segments) {
        const unique = segments.filter((x, i) => segments.indexOf(x) == i)
        return unique.length == 1
    }
}

function matches(rel, patterns) {
    let found = false
    for (let pattern of patterns) {
        if (pattern.startsWith('!')) {
            if (minimatch(rel, pattern.substring(1)))
                return false
            continue
        }

        found = found || minimatch(rel, pattern)
    }

    return found
}

